import React, {Component} from 'react';
import { ToastContainer, toast } from 'react-toastify';
import Coin from '../components/Coin';
import Wallet from '../components/Wallet';
import Spinner from '../components/Spinner/Spinner';
import arweave from '../arweave-config';
import 'react-toastify/dist/ReactToastify.css';

class DollarCostAverage extends Component {
    state = {
        coins: [],
        balance: "",
        wallet_address: null,
        jwk: null,
        loading: false
    }

    balance = 0;

    constructor(props) {
        super(props);
        this.onAddTransaction = this.onAddTransaction.bind(this);
        this.onRemoveTransaction = this.onRemoveTransaction.bind(this);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangePrice = this.onChangePrice.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onDeleteCoin = this.onDeleteCoin.bind(this);
    }

    componentWillMount() {    
        const wallet_address = sessionStorage.getItem('AR_Wallet', null);
        const jwk = JSON.parse(sessionStorage.getItem('AR_jwk', null));  

        this.setState({wallet_address: wallet_address, jwk: jwk});
        this.loadWallet(wallet_address);
    }

    loadWallet(wallet_address) {
        const that = this;

        if(wallet_address) {
            arweave.wallets.getBalance(wallet_address).then((balance) => {
                let ar = arweave.ar.winstonToAr(balance);

                const state = {balance: ar};

                that.setState(state);

                that.loadSavedCoins();
            });   
        }     
    }

    setWalletAddress(wallet_address_files) {
        const that = this;

        const reader = new FileReader();
        reader.onload = function() {
            const text = reader.result;
            const jwk = JSON.parse(text);

            arweave.wallets.jwkToAddress(jwk).then((wallet_address) => {                
                that.setState({wallet_address: wallet_address, jwk: jwk});
                sessionStorage.setItem('AR_Wallet', wallet_address);
                sessionStorage.setItem('AR_jwk', JSON.stringify(jwk));
            
                that.loadWallet(wallet_address);
            });
            
        }
        reader.readAsText(wallet_address_files[0]);
  
    }

    disconnectWallet() {
        sessionStorage.removeItem('AR_Wallet');
        sessionStorage.removeItem('AR_jwk');
        this.setState({coins: [], wallet_address: null, jwk: null, balance: 0});

        this.addSuccessAlert("Your wallet is now disconnected");
    }

    async loadSavedCoins() {
        this.setState({loading: true});

        const txids = await arweave.arql({
            op: "and",
            expr1: {
                op: "equals",
                expr1: "from",
                expr2: this.state.wallet_address
            },
            expr2: {
                op: "equals",
                expr1: "app",
                expr2: "arweave-dca"
            }
        });

        const that = this;
        
        let last_transaction_data = null;
        let current_transaction_data = null;
        let last_timestamp = 0;
        let current_timestamp = 0;

        for(let i in txids) {
            const txid = txids[i];
            
            arweave.transactions.get(txid).then(transaction => {
                const tags = transaction.get('tags');
                
                for(let i in tags) {
                    const tag = tags[i];
                    
                    const name = tag.get('name', {decode: true, string: true});
                    const value = parseInt(tag.get('value', {decode: true, string: true}));

                    if(name === "created" && value > last_timestamp) {
                        last_timestamp = current_timestamp;
                        current_timestamp = value;
                        current_transaction_data = transaction.get('data', {decode: true, string: true});
                    }
                };
            }).finally(() => {     
                const coins = JSON.parse(current_transaction_data);
                

                if(coins && current_timestamp > last_timestamp) {
                    that.setState({coins: coins});
                }

                that.setState({loading: false});
            });
        }       
    }

    async saveCoins() {
        if(this.state.balance <= 0) {
            this.addErrorAlert("You do not have enough AR to save your progress.");
            return;
        }

        if(this.state.coins.length === 0) {
            this.addErrorAlert("You do not have any coins configured!");
            return;
        }

        if(window.confirm("Are you sure you want to save the current coins?") === false) {
            return;
        }

        console.log(this.state.jwk);
        
        const coin_string = JSON.stringify(this.state.coins);
        let transaction = await arweave.createTransaction({
            data: coin_string
        }, this.state.jwk);

        transaction.addTag('Content-Type', 'application/json');
        transaction.addTag('app', 'arweave-dca');
        transaction.addTag('created', new Date().getTime());

        await arweave.transactions.sign(transaction, this.state.jwk);

        const response = await arweave.transactions.post(transaction);

        if(response.status === 200) {
            this.addSuccessAlert("Your coins were successfully saved!");
        } else if (response.status === 400) {
            this.addErrorAlert("There was a problem saving your coins.");
            console.log("Invalid transaction!");
        } else {
            this.addErrorAlert("There was a problem saving your coins.");
            console.log("Fatal error!");
        }

        this.loadWallet(this.state.wallet_address);
    } 

    addSuccessAlert(message)  {
        toast(message, { type: toast.TYPE.SUCCESS });     
    }

    addErrorAlert(message) {
        toast(message, { type: toast.TYPE.ERROR });  
        
    }

    onAddCoin() {
        const coins = [...this.state.coins];
        const coin_id = this.state.coins.length + 1;
        coins.push({
            id: coin_id,
            name: "",
            transactions: [{
                price: "",
                amount: ""
            }]
        });

        this.setState({coins: coins});
    }

    onDeleteCoin(coin_id) {
        const coins = [];
        
        for(let i in this.state.coins) {
            const coin = this.state.coins[i];

            if(coin.id !== coin_id) {
                coins.push(coin);
            }
        }

        this.setState({coins: coins});
    }

    onChangeName(coin_id, name) {
        const coins = [];
        for(let i in this.state.coins) {
            const coin = this.state.coins[i];

            if(coin.id === coin_id) {
                coin.name = name;
            }

            coins.push(coin);
        }

        this.setState({coins: coins});
    }

    onAddTransaction(coin_id) {
        const coins = [];
        for(let i in this.state.coins) {
            const coin = this.state.coins[i];

            if(coin.id === coin_id) {
                const transactions = [...coin.transactions];
                transactions.push({
                    price: "",
                    amount: ""
                });

                coin.transactions = transactions;
            }

            coins.push(coin);
        }

        this.setState({coins: coins});
    }

    onRemoveTransaction(coin_id, index) {
        const coins = [];
        for(let i in this.state.coins) {
            const coin = this.state.coins[i];

            if(coin.id === coin_id) {
                const transactions = [];

                for(let j in coin.transactions) {
                    const transaction = coin.transactions[j];
                    if(j !== index) {
                        transactions.push(transaction);
                    }
                }                

                coin.transactions = transactions;
            }

            coins.push(coin);
        }

        this.setState({coins: coins});
    }
    
    onChangePrice(coin_id, index, price) {
        const coins = [];
        for(let i in this.state.coins) {
            const coin = this.state.coins[i];

            if(coin.id === coin_id) {
                const transactions = [...coin.transactions];
                
                for(let j in transactions) {
                    const transaction = transactions[j];

                    if(j === index) {
                        transaction.price = price;
                    }
                }

                coin.transactions = transactions;
            }

            coins.push(coin);
        }

        this.setState({coins: coins});
    }

    onChangeAmount(coin_id, index, amount) {
        const coins = [];
        for(let i in this.state.coins) {
            const coin = this.state.coins[i];

            if(coin.id === coin_id) {
                const transactions = [...coin.transactions];
                
                for(let j in transactions) {
                    const transaction = transactions[j];

                    if(j === index) {
                        transaction.amount = amount;
                    }
                }

                coin.transactions = transactions;
            }

            coins.push(coin);
        }

        this.setState({coins: coins});
    }

    render() {
        let coins = this.state.loading ? <Spinner /> : null;

        if(this.state.coins.length > 0) {
            coins = [];
        }

        for(let i in this.state.coins) {
            const coin = this.state.coins[i];
            coins.push(
                <Coin 
                    key={"coin-" + i}
                    name={coin.name} 
                    id={coin.id} 
                    transactions={coin.transactions} 
                    onAddTransaction={this.onAddTransaction} 
                    onRemoveTransaction={this.onRemoveTransaction}
                    onChangeName={this.onChangeName}
                    onChangePrice={this.onChangePrice}
                    onChangeAmount={this.onChangeAmount}
                    onDeleteCoin={this.onDeleteCoin}
                />
            )
        }

        return (<>
            <ToastContainer />
            <div className="col-md-12">
                <div className="col-md-12 margin-top-20">
                    <div className="row">
                        
                        <div className="col-md-12 pull-right" >
                            <Wallet 
                                setWalletAddress={this.setWalletAddress.bind(this)} 
                                disconnectWallet={this.disconnectWallet.bind(this)}
                                saveCoins={this.saveCoins.bind(this)}
                                balance={this.state.balance}
                            />
                            <hr />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12 margin-top-20">
                            <h2>Coins</h2>
                        </div>
                        <div className="col-md-12">
                            <button onClick={this.onAddCoin.bind(this)} className="btn btn-primary">Add a coin +</button>
                            <button 
                                className="btn btn-success margin-left-10"
                                onClick={this.saveCoins.bind(this)}
                            >Save Coins</button>
                            <hr />
                        </div>
                        
                    </div>
                    
                </div>
                <div className="col-md-12 margin-top-20">
                    {coins}
                </div>
            </div>
            </>
        );
    }
}

export default DollarCostAverage;