import React from 'react';
import './App.css';
import DollarCostAverage from './containers/DollarCostAverager';

function App() {
  return (
    <DollarCostAverage />
  );
}

export default App;
