import React, {Component} from 'react';

class Wallet extends Component {
    status = {
        wallet_address: null
    }

    onChange(event) {
        const wallet_address_files = event.target.files;
        // this.setState({wallet_address: wallet_address});
        
        this.props.setWalletAddress(wallet_address_files);
    }

    disconnectWallet() {
        this.props.disconnectWallet();
    }

    saveCoins() {

    }

    render() {
        let wallet = (<>
            <label>Add your wallet address to enable save features:</label>
            <input 
                type="file" 
                className="margin-left-10"
                placeholder="AR Wallet Address" 
                title="Add your wallet address to enable save features" 
                onChange={(e) => {this.onChange(e)}}
            />
            <p>Need some free AR Tokens? <a href="https://tokens.arweave.org/" target="_blank">Click here!</a></p>
        </>);

        if(this.props.balance) {
            if(parseFloat(this.props.balance) > 0) {
                wallet = (<>
                    <h3>AR Balance: {this.props.balance}</h3>
                    <button 
                        className="btn btn-danger"
                        onClick={this.disconnectWallet.bind(this)}
                    >Disconnect</button>
                </>);
            }            
        }

        return (
            wallet
        );
    }
}

export default Wallet;