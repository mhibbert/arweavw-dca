import React, {Component} from "react";

class Coin extends Component {

    state = {
        transactions: [],
        dca: "(Please add transactions to calculate you DCA)"
    }

    constructor(props) {
        super(props);
        this.onRemoveTransaction = this.onRemoveTransaction.bind(this);
    }

    componentDidMount() {
        this.setState({transactions: this.props.transactions});
        this.calculateDCA(this.props.transactions);
    }

    componentDidUpdate(prevProps) {
        if(this.props.transactions !== undefined && this.props.transactions !== prevProps.transactions) {
            this.setState({transactions: this.props.transactions});
            this.calculateDCA(this.props.transactions);
        }
    }

    onAddTransaction() {
        this.props.onAddTransaction(this.props.id);
    }

    onRemoveTransaction(index) {
        this.props.onRemoveTransaction(this.props.id, index);
    }

    changeName(event) {
        const name = event.target.value;

        this.props.onChangeName(this.props.id, name);
    }

    calculateDCA(transactions) {
        let totalCost = 0;
        let totalAmount = 0;
        for(let i in transactions)  {
            const transaction = transactions[i];

            if(transaction.price !== "" && transaction.amount !== "") {
                totalCost += parseFloat(transaction.price) * parseFloat(transaction.amount);
                totalAmount += parseFloat(transaction.amount);
            }            
        }
        
        const dca = totalCost / totalAmount;
        
        if(!isNaN(dca)) {
            this.setState({dca: dca});
        }
        
    }

    onChangePrice(event, index) {
        const price = event.target.value;
        this.props.onChangePrice(this.props.id, index, price);
    }

    onChangeAmount(event, index) {
        const amount = event.target.value;
        this.props.onChangeAmount(this.props.id, index, amount);
    }

    onDeleteCoin() {
        if(!window.confirm("Are you sure you want to delete coin '" + this.props.name + "' ?")) {
            return;
        }

        this.props.onDeleteCoin(this.props.id);
    }

    render() {
        let transactions = [];
        const coin_name = this.props.name;
        const card_coin_name = coin_name !== "" ? coin_name : "Untitled"

        for(let i in this.state.transactions) {
            const transaction = this.state.transactions[i];
            transactions.push(

                <div className="row margin-bottom-5" key={i + this.props.id + "-transaction"}>
                    <div className="col-md-4">
                        <input 
                            type="text" 
                            placeholder="Price" 
                            defaultValue={transaction.price} 
                            className="form-control" 
                            onChange={(e) => {this.onChangePrice(e, i)}}
                        />
                    </div>
                    <div className="col-md-4">
                        <input 
                            type="text" 
                            placeholder="Amount" 
                            defaultValue={transaction.amount} 
                            className="form-control" 
                            onChange={(e) => {this.onChangeAmount(e, i)}}
                        />
                        
                    </div>
                    <div className="col-md-2">
                        <button className="btn btn-danger" onClick={() => this.onRemoveTransaction(i)}>Delete</button>
                    </div>
                </div>

            );
        }

        return (
            <div className="col-md-8 card margin-bottom-10">
                
                <div className="card-body">
                    <div className="card-title">
                        <strong>Break even at: {this.state.dca}</strong>
                    </div>
                    <div>
                        <input type="text" placeholder="Coin pair e.g. USDT/BTC" className="form-control" defaultValue={coin_name} onChange={(e) => {this.changeName(e)}}/>
                    </div>
                    <div className="padding-bottom-20 margin-top-20">
                        {transactions}                        
                    </div>
                    
                    <div className="margin-top-20">
                        <button className="btn btn-danger" onClick={this.onDeleteCoin.bind(this)}>Delete -</button>
                        <button className="btn btn-primary pull-right" onClick={this.onAddTransaction.bind(this)}>Add Transaction +</button>
                        <span className="margin-left-10"></span>
                    </div>
                </div>
            </div>
        );
    }
}

export default Coin;