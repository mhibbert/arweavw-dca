import React from 'react';

import classes from './Spinner.css';

const spinner = (props) => (
    <div style={{textAlign: 'center', width: '100%'}}>Loading Coins From Arweave Blockchain ...</div>
);

export default spinner;
